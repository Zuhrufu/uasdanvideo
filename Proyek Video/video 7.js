import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

export default class App extends React.Component{

    constructor(){
        super();
        this.state = {
            number: 0,
            Text:""
        }
    }

    componentDidMount(){
        this.setState({
            number: this.state.number + 1
        })    
    }

    handleClick(myName){
        this.setState({
            number: this.state.number + 1,
            Text:"Hello React Native"
        })    
    }

    render(){
        const myName ="Habib Shobriansyah";

        return(
        <View>
                <Text>Please Click Button Below</Text>
                <Text>{this.state.Text}</Text>
                <TouchableOpacity onPress={()=>this.handleClick(myName)}>
                <Text>Click</Text>
                </TouchableOpacity>
        </View>
        )
    }
}
